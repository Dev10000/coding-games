/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

 const n = parseInt(readline()); // the number of temperatures to analyse
 let inputs = readline().split(' ');

function closestToZero(inputs) {
    
    if(inputs == '' ) {
    return 0;  
    }
    
    let sortPositives = inputs
    .filter(e => e > 0)
    .sort((a, b) => a - b)[0];

    let sortNegatives = inputs
    .filter(e => e < 0)
    .sort((a, b) => b - a)[0];
    
// Olisi pitänyt käyttää Math.min ja Math.max, niin ei olisi tarvinnut tehdä alla olevaa ylimääräistä type checkkiä
    if(!sortNegatives) {
        sortNegatives = Infinity
    }

    if (sortPositives <= Math.abs(sortNegatives)) {
        return sortPositives;
    }
    return sortNegatives;  
}

console.log(closestToZero(inputs));